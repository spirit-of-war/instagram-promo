<?php

use yii\db\Migration;

/**
 * Handles the creation of table `posts`.
 */
class m180128_203035_create_posts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('posts', [
            'id' => $this->primaryKey(),
            'original_post_id' => $this->integer(),
            'type' => $this->string(255),
            'actions' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('posts');
    }
}
