<?php

use yii\db\Migration;

/**
 * Class m180130_084421_add_index_followers
 */
class m180130_084421_add_index_followers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('name_index', 'followers', 'account', true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('name_index', 'followers');
    }

}
