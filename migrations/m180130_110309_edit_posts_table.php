<?php

use yii\db\Migration;

/**
 * Class m180130_110309_edit_posts_table
 */
class m180130_110309_edit_posts_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('posts','original_post_id','string');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {

    }


}
