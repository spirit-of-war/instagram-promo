<?php

use yii\db\Migration;

/**
 * Class m180130_102804_add_index_posts
 */
class m180130_102804_add_index_posts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createIndex('original_post_index', 'posts', 'original_post_id', true);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropIndex('original_post_index', 'posts');
    }
}
