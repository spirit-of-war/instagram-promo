<?php

use yii\db\Migration;

/**
 * Handles the creation of table `log_scan`.
 */
class m180128_203010_create_log_scan_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('log_scan', [
            'id' => $this->primaryKey(),
            'datetime' => $this->integer(),
            'account' => $this->string(255),
            'followers' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('log_scan');
    }
}
