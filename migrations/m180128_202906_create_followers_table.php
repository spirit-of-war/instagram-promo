<?php

use yii\db\Migration;

/**
 * Handles the creation of table `followers`.
 */
class m180128_202906_create_followers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('followers', [
            'id' => $this->primaryKey(),
            'account' => $this->string(255),
            'date_in' => $this->integer(),
            'date_out' => $this->integer(),
            'active' => $this->integer(1),
            'status_scan' => $this->integer(1),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('followers');
    }
}
