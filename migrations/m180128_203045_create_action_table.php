<?php

use yii\db\Migration;

/**
 * Handles the creation of table `action`.
 */
class m180128_203045_create_action_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('action', [
            'id' => $this->primaryKey(),
            'datetime' => $this->integer(),
            'post_id' => $this->integer(),
            'followers' => $this->integer(),
            'other_users' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('action');
    }
}
