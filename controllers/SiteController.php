<?php

namespace app\controllers;

use app\models\Followers;
use app\models\InstagramQuery;
use app\models\LogScan;
use app\models\Posts;
use InstagramScraper\Exception\InstagramNotFoundException;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\grid\GridView;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use \InstagramScraper\Instagram;

class SiteController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionStartScan($account = 'ddborisov')
    {
        $message = 'Сканирование завершено';
        try {
            $instagramQuery = new InstagramQuery($account);
            $instagramQuery->startScan();
        } catch (InstagramNotFoundException $e) {
            $message = 'Ошибка: Такого аккаунта не существует';
        }

        return $this->render('startScan',[
            'message' => $message
        ]);
    }

    public function actionScanInfo()
    {
        $queryFollowers = Followers::find();
        $queryPosts = Posts::find();
        $queryLog = LogScan::find();

        $postsProvider = $this->getProvider($queryPosts);
        $followersProvider = $this->getProvider($queryFollowers);
        $logScanProvider = $this->getProvider($queryLog);

        return $this->render('scanInfo', [
            'postsProvider' => $postsProvider,
            'followersProvider' => $followersProvider,
            'logScanProvider' => $logScanProvider,
        ]);
    }

    private function getProvider($query)
    {
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);
    }

}
