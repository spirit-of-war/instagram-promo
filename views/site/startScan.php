<?php

use \yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';

?>
<div class="site-index">
    <b><?= $message ?></b>
    <br><br>
    <?= Html::a('Посмотреть результаты', '/site/scan-info', ['class' => 'btn btn-primary pull-right']) ?>

</div>