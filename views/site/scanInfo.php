<?php

use \yii\grid\GridView;
use \yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';

?>
<div class="site-index">
    <b>Результаты сканирования</b>
    <br><br>

    <?= Html::a('Сканировать еще раз', '/site/start-scan', ['class' => 'btn btn-primary pull-right']) ?>

    Посты:
    <?= GridView::widget([
        'dataProvider' => $postsProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'original_post_id',
            'type',
            'actions',
        ],
    ]); ?>

    Подписчики:
    <?= GridView::widget([
        'dataProvider' => $followersProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'account',
            'date_in:datetime',
            'date_out:datetime',
            'active'
        ],
    ]); ?>

    Логи сканирования:
    <?= GridView::widget([
        'dataProvider' => $logScanProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'datetime:datetime',
            'account',
            'followers',
        ],
    ]); ?>
</div>