<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "followers".
 *
 * @property int $id
 * @property string $account
 * @property int $date_in
 * @property int $date_out
 * @property int $active
 * @property int $status_scan
 */
class Followers extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_SCAN_PROCESSING = 1;
    const STATUS_SCAN_DONE = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'followers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_in', 'date_out', 'active','status_scan'], 'integer'],
            [['account'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account' => 'Account',
            'date_in' => 'Date In',
            'date_out' => 'Date Out',
            'active' => 'Active',
            'status_scan' => 'Status Scan',
        ];
    }

    public static function batchInsertOrUpdate($followersInfo)
    {
        $db = Yii::$app->db;

        foreach ($followersInfo as $infoChunk) {
            $sql = $db->queryBuilder->batchInsert(Followers::tableName(), ['account','date_in','active','status_scan'], $infoChunk);
            $sql .= ' ON DUPLICATE KEY UPDATE status_scan=' . self::STATUS_SCAN_DONE;
            $db->createCommand($sql)->execute();
        }

    }

    public static function batchUpdate($followersInfo)
    {
        foreach ($followersInfo as $infoChunk) {
            Yii::$app->db->createCommand()->batchInsert(Followers::tableName(), ['account','date_in','active','status_scan'], $infoChunk)->execute();
        }
    }

}
