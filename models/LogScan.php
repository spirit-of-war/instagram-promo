<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_scan".
 *
 * @property int $id
 * @property int $datetime
 * @property string $account
 * @property int $followers
 */
class LogScan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_scan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datetime', 'followers'], 'integer'],
            [['account'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datetime' => 'Datetime',
            'account' => 'Account',
            'followers' => 'Followers',
        ];
    }

    public static function saveLog($account,$followersCount)
    {
        $model = new LogScan();
        $model->account = $account;
        $model->followers = $followersCount;
        $model->datetime = time();

        return $model->save();
    }
}
