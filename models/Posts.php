<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property string $original_post_id
 * @property string $type
 * @property int $actions
 */
class Posts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['actions'], 'integer'],
            [['original_post_id','type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'original_post_id' => 'Original Post ID',
            'type' => 'Type',
            'actions' => 'Actions',
        ];
    }

    public static function batchInsertOrUpdate($postsInfo)
    {
        $db = Yii::$app->db;
        $sql = $db->queryBuilder->batchInsert(Posts::tableName(), ['original_post_id','type','actions'], $postsInfo);
        $sql .= ' ON DUPLICATE KEY UPDATE actions=VALUES(actions)';
        $db->createCommand($sql)->execute();

    }
}
