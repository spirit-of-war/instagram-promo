<?php

namespace app\models;

use InstagramScraper\Instagram;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class InstagramQuery
{

    const MAX_POSTS = 30;
    const PAGE_SIZE = 2000;
    const ARRAY_CHUNK_SIZE = 1000;


    protected $username;
    protected $instagramm;
    protected $account;

    function __construct($username)
    {
        $this->username = $username;
        $this->instagram = $this->connectInst(Yii::$app->params['instagram']['username'], Yii::$app->params['instagram']['password']);
        $this->account = $this->getInstAccount($this->instagram, $this->username);
    }

    public function startScan()
    {

        $this->scanFollowers();
        $this->scanPosts();
        LogScan::saveLog($this->account->getUsername(),$this->account->getFollowedByCount());

    }

    protected function scanFollowers()
    {
        Followers::updateAll(['status_scan' => Followers::STATUS_SCAN_PROCESSING]);

        $followersInfo = $this->getFollowersInfo($this->instagram, $this->account->getId(), $this->account->getFollowedByCount());

        Followers::batchInsertOrUpdate($followersInfo);

        $time = time();
        Followers::updateAll(
            [
                'status_scan' => Followers::STATUS_SCAN_DONE,
                'date_out' => $time,
                'active' => Followers::STATUS_INACTIVE
            ],
            'status_scan=' . Followers::STATUS_SCAN_PROCESSING . ' AND date_out IS NULL'
        );
    }

    protected function scanPosts()
    {
        $postsInfo = $this->getPostsInfo($this->instagram, $this->username);
        Posts::batchInsertOrUpdate($postsInfo);
    }

    private function getPostsInfo(Instagram $instagram, $username)
    {
        $medias = $instagram->getMedias($username, static::MAX_POSTS);

        $mediasInfo = [];

        foreach ($medias as $media) {
            $mediasInfo[] = [
                $media->getId(),
                $media->getType(),
                $media->getLikesCount()
            ];
        }

        return $mediasInfo;
    }


    private function connectInst($username, $password)
    {
        $instagram = Instagram::withCredentials($username, $password);
        $instagram->login();
        sleep(2); // Delay to mimic user

        return $instagram;
    }

    private function getInstAccount(Instagram $instagram, $username)
    {
        $account = $instagram->getAccount($username);
        sleep(1);

        return $account;
    }

    private function getFollowersInfo(Instagram $instagram, $accountId ,$followedCount = 10000)
    {
        $followers = $instagram->getFollowers($accountId, $followedCount, static::PAGE_SIZE);

        $followersInfo = [];
        $time = time();
        foreach ($followers as $follower) {
            $followersInfo[] = [
                $follower['username'],
                $time,
                Followers::STATUS_ACTIVE,
                Followers::STATUS_SCAN_DONE
            ];
        }
        $followersInfo = array_chunk($followersInfo, static::ARRAY_CHUNK_SIZE);

        return $followersInfo;
    }


}